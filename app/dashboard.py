import json
import os.path

import boto3
import gradio as gr
import pandas as pd
import urllib3

import lawfulness

sagemaker_runtime_client = boto3.client('sagemaker-runtime', region_name="eu-west-1")
sagemaker_client = boto3.client('sagemaker', region_name="eu-west-1")
ssm_client = boto3.client('ssm', region_name="eu-west-1")

config = {
    "multi-label-division-classifier": {
        "ssm_path": "/tedai/sagemaker/endpoint/multi_label_division_classifier/name"
    },
    "contract-budgetary-values-extractor-classifier": {
        "ssm_path": "/tedai/sagemaker/endpoint/budgetary_value_classifier/name"
    },
    "opentender-multi-label-division-classifier": {
        "ssm_path": "/tedai/sagemaker/endpoint/opentender_multi_label_division_classifier/name"
    }
}


def model_multi_label_division_classifier(title: str, description: str) -> str:
    endpoint_name = config["multi-label-division-classifier"]['endpoint_name']
    print(f"Sending title '{title}' and description '{description}' to endpoint '{endpoint_name}'")

    payload = {
        "title": title,
        "description": description
    }
    response = sagemaker_runtime_client.invoke_endpoint(
        EndpointName=endpoint_name,
        Body=json.dumps(payload),
        ContentType='application/json'
    )
    result = json.loads(response["Body"].read().decode())
    print(result)
    s1 = json.dumps(result)
    data = json.loads(s1)
    json_data = pd.json_normalize(data['predictions'])
    print(json_data)
    return json_data


def model_opentender_multi_label_division_classifier(title: str, description: str) -> str:
    endpoint_name = config["opentender-multi-label-division-classifier"]['endpoint_name']
    print(f"Sending title '{title}' and description '{description}' to endpoint '{endpoint_name}'")

    payload = {
        "title": title,
        "description": description
    }
    response = sagemaker_runtime_client.invoke_endpoint(
        EndpointName=endpoint_name,
        Body=json.dumps(payload),
        ContentType='application/json'
    )

    result = json.loads(response["Body"].read().decode())
    print(result)
    s1 = json.dumps(result)
    data = json.loads(s1)
    json_data = pd.json_normalize(data['predictions'])
    print(json_data)
    return json_data


def model_budgetary_value_classifier_json(text: str) -> str:
    endpoint_name = config["contract-budgetary-values-extractor-classifier"]['endpoint_name']
    print(f"Sending text to endpoint '{endpoint_name}'")

    payload = {
        "text": text
    }
    response = sagemaker_runtime_client.invoke_endpoint(
        EndpointName=endpoint_name,
        Body=json.dumps(payload),
        ContentType='application/json'
    )
    result = json.loads(response["Body"].read().decode())
    s1 = json.dumps(result, indent=3)
    data = json.loads(s1)
    json_data = pd.json_normalize(data)
    print(json_data)
    return json_data


def model_budgetary_value_classifier_file(file) -> str:
    endpoint_name = config["contract-budgetary-values-extractor-classifier"]['endpoint_name']
    actual_filename = os.path.basename(file.name)
    print(f"Sending {actual_filename} to endpoint '{endpoint_name}'")

    payload, content_type = urllib3.encode_multipart_formdata({
        "file": (actual_filename, open(file.name, "rb").read())
    })

    print(f"Encoded payload of content-type: {content_type}")
    response = sagemaker_runtime_client.invoke_endpoint(
        EndpointName=endpoint_name,
        Body=payload,
        ContentType=content_type
    )
    result = json.loads(response["Body"].read().decode())
    s1 = json.dumps(result, indent=3)
    data = json.loads(s1)
    json_data = pd.json_normalize(data)
    print(json_data)
    return json_data


def get_endpoint_state(endpoint_name: str) -> str:
    try:
        response = sagemaker_client.describe_endpoint(EndpointName=endpoint_name)
        return response['EndpointStatus']
    except:
        return "OutOfService"


def manage_endpoint(action: str, endpoint_name: str) -> None:
    endpoint_state = get_endpoint_state(endpoint_name)

    if action == "start":
        if endpoint_state == "InService":
            print(f"Endpoint {endpoint_name} is already running, nothing to do.")
        else:
            sagemaker_client.create_endpoint(
                EndpointName=endpoint_name,
                EndpointConfigName=f"{endpoint_name}-config"
            )
    elif action == "stop":
        sagemaker_client.delete_endpoint(EndpointName=endpoint_name)
    else:
        print(f"Action '{action}' not managed, nothing to do.")


def update_markdown_state_for_budget_endpoint(r: gr.Request):
    endpoint_name = config["contract-budgetary-values-extractor-classifier"]['endpoint_name']
    endpoint_state = get_endpoint_state(endpoint_name)
    return f"The state of the endpoint is : **{endpoint_state}**"


def update_markdown_state_for_cpv_endpoint(r: gr.Request):
    endpoint_name = config["multi-label-division-classifier"]['endpoint_name']
    endpoint_state = get_endpoint_state(endpoint_name)
    return f"The state of the endpoint is : **{endpoint_state}**"


def update_markdown_state_for_cpv_opentender_endpoint(r: gr.Request):
    endpoint_name = config["opentender-multi-label-division-classifier"]['endpoint_name']
    endpoint_state = get_endpoint_state(endpoint_name)
    return f"The state of the endpoint is : **{endpoint_state}**"


if __name__ == '__main__':
    for classifier in config.items():
        classifier_name, classifier_config = classifier
        ssm_path = classifier_config["ssm_path"]
        response = ssm_client.get_parameter(Name=ssm_path)
        endpoint_name = response['Parameter']['Value']
        print(f"Adding endpoint '{endpoint_name}' to classifier '{classifier_name}'")
        config[classifier_name]["endpoint_name"] = endpoint_name

    print(f"Finished updating config: {config}")

    with gr.Blocks(title="Dashboard - TED AI models demo") as demo:
        gr.Markdown("<center><h1>Dashboard - TED AI models demo</h1></center>")
        with gr.Tab("MultiLabel Division Classifier"):
            # First model: multi-label-division-classifier
            gr.Markdown("# <u>multi-label-division-classifier</u>")
            gr.Markdown("## Model based on EU notices")
            gr.Markdown("This model expects a title and a description, then returns predictions for CPV.")
            gr.Markdown("**If the state of the endpoint is not InService, you need to start it with the button"
                        "'Start endpoint' below and wait for it to be in state 'InService'. "
                        "Refresh the page to check on the current state.**")
            cpv_classifier_endpoint_name = config["multi-label-division-classifier"]["endpoint_name"]
            state_markdown = gr.Markdown()
            demo.load(update_markdown_state_for_cpv_endpoint, None, state_markdown)

            with gr.Row():
                endpoint_name_textbox = gr.Textbox(value=cpv_classifier_endpoint_name, visible=False)
                with gr.Column():
                    start_endpoint_button = gr.Button("Start endpoint")
                    start_textbox = gr.Textbox(value="start", visible=False)
                    start_endpoint_button.click(manage_endpoint, inputs=[start_textbox, endpoint_name_textbox],
                                                outputs=None)
                demo.load(update_markdown_state_for_cpv_endpoint, None, state_markdown)
                with gr.Column():
                    stop_endpoint_button = gr.Button("Stop endpoint")
                    stop_textbox = gr.Textbox(value="stop", visible=False)
                stop_endpoint_button.click(manage_endpoint, inputs=[stop_textbox, endpoint_name_textbox],
                                           outputs=None)
                demo.load(update_markdown_state_for_cpv_endpoint, None, state_markdown)

            with gr.Row():
                with gr.Column():
                    title_textbox = gr.Textbox(label="Title")
                    description_textbox = gr.Textbox(label="Description")
                with gr.Row():
                    with gr.Column():
                        gr.Markdown("Inference")
                        output_textbox = [gr.Dataframe(row_count=(3, "dynamic"), col_count=(2, "fixed"),
                                                       headers=['CPV Number', 'Label'], height=(200))]
            predict_button = gr.Button("Predict CPV")
            predict_button.click(model_multi_label_division_classifier, inputs=[title_textbox, description_textbox],
                                 outputs=output_textbox)
            gr.Markdown("## Model based on Opentender data")
            gr.Markdown("This model expects a title and a description, then returns predictions for CPV.")
            gr.Markdown("**If the state of the endpoint is not InService, you need to start it with the button"
                        "'Start endpoint' below and wait for it to be in state 'InService'. "
                        "Refresh the page to check on the current state.**")
            cpv_opentender_classifier_endpoint_name = config["opentender-multi-label-division-classifier"]["endpoint_name"]
            state_markdown = gr.Markdown()
            demo.load(update_markdown_state_for_cpv_opentender_endpoint, None, state_markdown)

            with gr.Row():
                endpoint_name_textbox = gr.Textbox(value=cpv_opentender_classifier_endpoint_name, visible=False)
                with gr.Column():
                    start_endpoint_button = gr.Button("Start endpoint")
                    start_textbox = gr.Textbox(value="start", visible=False)
                    start_endpoint_button.click(manage_endpoint, inputs=[start_textbox, endpoint_name_textbox],
                                                outputs=None)
                demo.load(update_markdown_state_for_cpv_opentender_endpoint, None, state_markdown)
                with gr.Column():
                    stop_endpoint_button = gr.Button("Stop endpoint")
                    stop_textbox = gr.Textbox(value="stop", visible=False)
                stop_endpoint_button.click(manage_endpoint, inputs=[stop_textbox, endpoint_name_textbox],
                                           outputs=None)
                demo.load(update_markdown_state_for_cpv_opentender_endpoint, None, state_markdown)

            with gr.Row():
                with gr.Column():
                    title_textbox = gr.Textbox(label="Title")
                    description_textbox = gr.Textbox(label="Description")
                with gr.Row():
                    with gr.Column():
                        gr.Markdown("Inference")
                        output_textbox = [gr.Dataframe(row_count=(3, "dynamic"), col_count=(2, "fixed"),
                                                       headers=['CPV Number', 'Label'], height=(200))]
            predict_button = gr.Button("Predict CPV")
            predict_button.click(model_opentender_multi_label_division_classifier, inputs=[title_textbox, description_textbox],
                                 outputs=output_textbox)

        with gr.Tab("Extract Contract Budgetary Values"):
            # Second model: contract-budgetary-values-extractor-classifier with text data
            gr.Markdown("## <u>contract-budgetary-values-extractor-classifier</u>")

            gr.Markdown("**If the state of the endpoint is not InService, you need to start it with the button"
                        "'Start endpoint' below and wait for it to be in state 'InService'. "
                        "Refresh the page to check on the current state.**")
            budget_endpoint_name = config["contract-budgetary-values-extractor-classifier"]["endpoint_name"]
            state_markdown = gr.Markdown()
            demo.load(update_markdown_state_for_budget_endpoint, None, state_markdown)

            with gr.Row():
                endpoint_name_textbox = gr.Textbox(value=budget_endpoint_name, visible=False)
                with gr.Column():
                    start_endpoint_button = gr.Button("Start endpoint")
                    start_textbox = gr.Textbox(value="start", visible=False)
                    start_endpoint_button.click(manage_endpoint, inputs=[start_textbox, endpoint_name_textbox],
                                                outputs=None)
                demo.load(update_markdown_state_for_budget_endpoint, None, state_markdown)
                with gr.Column():
                    stop_endpoint_button = gr.Button("Stop endpoint")
                    stop_textbox = gr.Textbox(value="stop", visible=False)
                stop_endpoint_button.click(manage_endpoint, inputs=[stop_textbox, endpoint_name_textbox], outputs=None)
                demo.load(update_markdown_state_for_budget_endpoint, None, state_markdown)

            gr.Markdown("## With text data")
            with gr.Row():
                with gr.Column():
                    gr.Markdown(
                        "This model expects a text with budgetary values, then returns the extracted budgetary value.")
                    cbve_text_textbox = gr.Textbox(label="Text")
                with gr.Column():
                    gr.Markdown("Inference")
                    cbve_text_output_textbox = [
                        gr.Dataframe(datatype=["number", "str", "str"], headers=['Probability', 'Value', 'Context'],
                                     row_count=(3, "dynamic"), col_count=(3, "fixed"), height=(200))]
                    cbve_text_predict_button = gr.Button("Extract budgetary values")
                    cbve_text_predict_button.click(model_budgetary_value_classifier_json,
                                                   inputs=[cbve_text_textbox],
                                                   outputs=cbve_text_output_textbox)

            # Third model: contract-budgetary-values-extractor-classifier with file upload
            gr.Markdown("## With file upload")
            gr.Markdown("This model expects a file (docx or pdf), then returns the extracted budgetary values "
                        "from the file")
            with gr.Row():
                with gr.Column():
                    cbve_file = gr.File()
            cbve_predict_button = gr.Button("Upload file for prediction")
            cbve_file_output_textbox = [gr.Dataframe(row_count=(3, "dynamic"), col_count=(3, "fixed"),
                                                     headers=['Probability', 'Value', 'Context'], label="Results",
                                                     height=(200))]
            cbve_predict_button.click(model_budgetary_value_classifier_file, inputs=cbve_file,
                                      outputs=cbve_file_output_textbox)

        lawfulness.display_tab()

demo.launch(server_name="0.0.0.0", server_port=7860)
